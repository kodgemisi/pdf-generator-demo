package com.example;

import com.kodgemisi.pdfgenerator.PdfGenerationOptions;
import com.kodgemisi.pdfgenerator.PdfGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

/**
 * -Dorg.slf4j.simpleLogger.defaultLogLevel=trace
 */
public class App {

	private static Logger log = LoggerFactory.getLogger(App.class);

	public static void main(String[] args) throws MalformedURLException {

		final PdfGenerator pdfGeneration = new PdfGenerator(false);

		final Path pdfFilePath = Paths.get(System.getProperty("user.home"), "Desktop", "trying.pdf");

		final Map<String, String> headers = new HashMap<>();
		headers.put("Authoriazation", "Bearer tRyInG_sOmEtHiNg1234567890");

		final PdfGenerationOptions options = PdfGenerationOptions
				.builder(pdfFilePath)
				.extraHttpHeaders(headers)
				.build();

		final CompletableFuture<Boolean> future = pdfGeneration.generatePdf(new URL("http://request.urih.com"), options);

		future
			.thenAccept(aBoolean -> {
				log.info("Done with " + aBoolean);
				System.exit(0);
			})
			.exceptionally(e-> {
				System.err.println("ERROR: " + e.getMessage());
				System.exit(0);
				return null;
			});

		log.info("Main thread exits.");

	}
}
